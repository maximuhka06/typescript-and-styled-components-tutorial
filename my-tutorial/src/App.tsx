import React, { useCallback, useState } from 'react';
import './App.css';
import styled from 'styled-components';

const dataTextContent = [{
  id: 1,
  text: 'Hi',
}, {
  id: 2,
  text: 'He',
}, {
  id: 3,
  text: 'We',
}, {
  id: 4,
  text: 'Ho',
}, {
  id: 5,
  text: 'No',
}];

export default function App() {
  const [refContainer, setRefContainer] = useState(null);
  const [widthContainer, setWidthContainer] = useState(0);

  const getWidthContainer = (ref: { offsetWidth: number; }) => {
    console.log(ref.offsetWidth);
    setWidthContainer(ref.offsetWidth / 4);
  };

  const getRefContainer = useCallback((ref) => {
    if (ref === null) return;
    setRefContainer(ref);
    getWidthContainer(ref);
  }, []);

  console.log(widthContainer);

  return (
    <Container ref={getRefContainer}>
      {dataTextContent.map((item) => (
        <TextContent key={item.id}>
          {item.text}
        </TextContent>
      ))}
      <div>{widthContainer}</div>
    </Container>
  );
}

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-wrap: wrap;
`;

const TextContent = styled.h1`
  font-size: 100px;
  line-height: 0;
  min-width: 200px;
  height: 200px;
  border-radius: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 
    -10px -10px 0px 0px rgba(0, 0, 0, 0.3),
    inset -10px -10px 0px 0px rgba(0, 0, 0, 0.7);
  background: linear-gradient(135deg, rgba(0, 0, 0, 0.7) 10%, rgba(0, 0, 0, 0) 90%);
  box-sizing: border-box;
  padding: 0 40px;
  margin-bottom: 0;
  align-self: flex-start;
  /* -webkit-background-clip: text; */
  /* -webkit-text-fill-color: transparent; */
  
  :not(:last-child) {
    margin-right: 40px;
  }
  :first-letter {
    color: #FFF;
    /* -webkit-text-fill-color: #FFF; */
  }
`;
